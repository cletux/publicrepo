import React, { Component } from 'react';
import CountryCard from './CountryCard';
import { loading } from './Loading';
import {connect} from 'react-redux';

class CountryCardList extends Component{
  constructor(props){
    super(props);
  }

  renderCountryList(){
    console.log('-----LIST-----');
    console.log(this.props);
    console.log('-------------');
    if( this.props.countries.isLoading === undefined || this.props.countries.isLoading ){
      let values={
        label:'Loading',
        size: 80,
        color:'#194d33'
      }
      return (
        <div className="load">
          <div className="col-4">
            {loading(values)}
          </div>
        </div>
        )
    }else{
      if( this.props.countries.isError ){
        return(
          <div>
            <h1>ERROR Loading</h1>
            <h1>Try later</h1>
          </div>
        )
        
      }else{
        if( this.props.listOfCountries === undefined ){
          return (
            <div>
              Sin definirse
            </div>
          )
        }else{
          return this.props.listOfCountries.map( (country, index) =>{
            let name = country.name;
            return (
              <div>
                <CountryCard  name={name} />
              </div>
            )  
          })
        }
      }
    }//
  }


  render(){
    return(
      <div className="container">
        {this.renderCountryList()}
      </div>
    )
  }

 }

const mapStateToProps = (reducers) =>{
  return {countries: reducers.countriesReducers}
}

export default connect(mapStateToProps)(CountryCardList);
 