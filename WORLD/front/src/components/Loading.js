import React from 'react';
import ClipLoader from "react-spinners/ClipLoader";
import '../css/loading.css';

 export function loading (props){
   console.log(props);
    return (
        <div className="comp-loading text-center py-5 "  >
          <ClipLoader 	
          size={props.size}
          color={props.color}
          
        />
          <h5>{props.label}</h5>
        </div>)
 }
        
  
