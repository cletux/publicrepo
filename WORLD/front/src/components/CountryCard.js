import React, {Component} from 'react';
import * as countryActions from '../actions/countries-actions';
import {connect} from 'react-redux';

class CountryCard extends Component{

    handleClick = () =>{
        
        console.log('state');
        console.log(this.props.country.countryHistory);
        this.props.setCountry(this.props.name);
    }

    render(){
        return(
            <div className="card" onClick={this.handleClick}>         
                <div className="card-body">
                <h4 className="card-title">{this.props.name}</h4>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (reducers) =>{
    return {country:reducers.countriesReducers};
  }

export default connect(mapStateToProps, countryActions )(CountryCard);
