import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as countryActions from '../actions/countries-actions';

class SelectedCountry extends Component{

    constructor(props){
        super(props);
    }
    

    renderCurrencies(){
        let country = {};
        if( this.props.country.countryDetail.length > 0 ){
            country = this.props.country.countryDetail[0];
        }
       if( country !== undefined && country.currencies !== undefined && country.currencies.length > 0 ){
        return country.currencies.map( value =>{
            return(
                <div>
                    <h7>{value}</h7>
                </div>
            )
        })
       }
    }

    render(){
        let country = {};
        if( this.props.country.countryDetail.length > 0 ){
            country = this.props.country.countryDetail[0];
        

        }
        return(
            <div>
                 <div>
                    <div>
                        <br></br>
                        <h7>Name: {country.name}</h7> 
                        <br></br>
                        <h7>Capital: {country.capital}</h7>
                        <br></br>
                        <h7>Currencie: {this.renderCurrencies()}</h7>
                        <h7>population: {country.population}</h7>
                        <br></br>
                        <h7>country region: {country.region}</h7>
                        <br></br>
                        <h7>subregion: {country.subregion}</h7>
                        <br></br>
                        <h7>language: {country.languages}</h7>
                    </div>
                    <div>
                        {country.population}
                    </div>
                    
                </div>
            </div>

            
        )
    }
    
}

const mapStateToProps = (reducers) =>{
    return { country: reducers.countriesReducers }
}

export default connect(mapStateToProps, countryActions)(SelectedCountry);