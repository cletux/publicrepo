import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as countryActions from '../actions/countries-actions';

class HistoryCountry extends Component{

    constructor( props ){
        super(props);
    }

    renderComp = () =>{
        let sortedArray = this.props.country.countryHistory.sort( function(a,b)  {
            if( a.name.toLowerCase() > b.name.toLowerCase()){
                return 1;
            }
            if( b.name.toLowerCase() > a.name.toLowerCase() ){
                return -1;
            }
            return 0;
        } )
        return sortedArray.map( value =>{
        return <h5>{value.name}{' '}clicks:{value.count}</h5>
        });
    }

    render(){
        
        return(
            <div className="hist-comp">
                {this.renderComp()}        
            </div>
        )
    }

}

const mapStateToProps = ( reducers ) =>{
    return {country: reducers.countriesReducers}
}

export default connect( mapStateToProps, countryActions )(HistoryCountry);