import React, {Component} from 'react';
import './App.css';
import CountryCardList from './components/CountryCardList';
import {connect} from 'react-redux';
import * as countryActions from './actions/countries-actions';
import SelectedContry from './components/SelectedCountry';
import './css/country.css';
import HistoryCoutry from './components/HistoryCoutry';
import mundo from './images/mundo.jpeg';

class App extends Component {


  constructor(props){
    super(props);
  }

  componentDidMount(){
    this.props.getAllCountries();
  }

  render(){
    let listOfCountries = this.props.country.listOfCountries; 
    return (
      <div className="App">
        <div className="row">
          <header className="col-12">
            <div className="title card">
              <h1>WORLD INFORMATION</h1>
            </div>
          </header>
        </div>
        <div className="container">
          <div className="row">
            <div className='left-comp'>
              <CountryCardList className='col-4'
                listOfCountries={listOfCountries}
              />
            </div>
            <div className="col-3 card">
              <SelectedContry/>
            </div>
            <div className="col-3 card">
              <HistoryCoutry />
            </div>
          </div>
        </div>
      </div>
    )  
  }

}

const mapStateToProps = (reducers) =>{
  return {country:reducers.countriesReducers};
}

export default connect(mapStateToProps, countryActions)(App);
