import {Actions} from '../actions/countries-actions';

const INITIAL_STATE = {
    listOfCountries:[],
    selectedCountry: 'none',
    countryDetail:[],
    countryHistory:[],
    isLoading: true,
    isError: false
};

export default(state= INITIAL_STATE, action)=>{
    switch(action.type){
        case Actions.Type.GET_ALL:
            return {...state, listOfCountries:action.payload}
        case Actions.Type.CHANGE_COUNTRY:
            let countryDetail = state.listOfCountries.filter( (country) =>{
                if( country.name === action.payload ){
                    return country
                }
            });
            return {...state, selectedCountry: action.payload, countryDetail: countryDetail}
        case Actions.Type.LOAD_DATA_CORRECT:
            return {...state, isLoading: false}
        case Actions.Type.LOAD_DATA_INCORRECT:
            return {...state, isLoading: false, isError: true}
        case Actions.Type.ADD_HISTORY:
           let newCount = state.countryHistory.find( (country) =>{
               return country.name === action.payload
           } )
            if( newCount !== undefined ){
                newCount = {name: newCount.name, count: newCount.count + 1}
            }else{
                newCount = {name: action.payload, count: 1}
            }
            let newItems = state.countryHistory.filter(hist => action.payload !== hist.name);
            let item = {name: newCount.name, count: newCount.count};
            return {...state, countryHistory: [...newItems, item]}
        default: return {...state};
    }
}