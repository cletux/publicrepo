import { combineReducers } from 'redux';
import countriesReducers from './countries-reducers';

export default combineReducers({
    countriesReducers
})