import axios from 'axios';

const petition = axios.create({
    baseURL: 'http://localhost:8088'
})

export function getCountries(){
    return petition.get('/countries',{});
}