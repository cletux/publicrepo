import {getCountries} from '../api/CountryInformationService';

let Actions = {};
Actions.Type ={
    GET_ALL:'GET_ALL',
    CHANGE_COUNTRY: 'CHANGE_COUNTRY',
    NUMBER_CLICK: 'NUMBER_CLICK',
    GET_CLICK: 'GET_CLICK',
    ADD_HISTORY: 'ADD_HISTORY',
    LOAD_DATA_CORRECT: 'LOAD_DATA_CORRECT',
    LOAD_DATA_INCORRECT: 'LOAD_DATA_INCORRECT'
}

export {Actions};

export const getAllCountries = () => async (dispatch) =>{
    try{
        const response = await getCountries();
         dispatch({
             type: Actions.Type.GET_ALL,
             payload: response.data
         });
         dispatch({
             type: Actions.Type.LOAD_DATA_CORRECT
         })
    }catch(error){
        console.log(error);
        dispatch({
            type: Actions.Type.LOAD_DATA_INCORRECT
        })
    }
}

export const setCountry = ( name ) => (dispatch) => {
    dispatch({
        type: Actions.Type.CHANGE_COUNTRY,
        payload: name
    })
    dispatch({
        type: Actions.Type.ADD_HISTORY,
        payload: name
    })
}