package com.example.demo.service;

import com.example.demo.dto.CountryDTO;

public interface WorldInfoService {
	public CountryDTO[] getCountries();

}
