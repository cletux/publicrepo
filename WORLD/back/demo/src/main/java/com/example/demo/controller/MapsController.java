package com.example.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CountryDTO;
import com.example.demo.service.RapidApiService;
import com.example.demo.service.WorldInfoService;

//@CrossOrigin(origins = "*")
@RestController
public class MapsController {
    
	//@CrossOrigin(origins = "http://localhost:3000")
	@CrossOrigin(origins = "*")
	@GetMapping("/countries")
	public CountryDTO[] execute() {
		WorldInfoService info = new RapidApiService();
		return info.getCountries();
	}

}
