package com.example.demo.service;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.CountryDTO;

public class RapidApiService implements WorldInfoService {
	
	public CountryDTO[] getCountries() {
		final String uri = "https://restcountries-v1.p.rapidapi.com/all";
	     
	    RestTemplate restTemplate = new RestTemplate();
	     
	    HttpHeaders headers = new HttpHeaders();

	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.set("x-rapidapi-host", "restcountries-v1.p.rapidapi.com"); // optional - in case you auth in headers
	    headers.set("x-rapidapi-key", "f97f6d3f79msh53189988a96aee4p142869jsn5d3de6efc5f9");
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<CountryDTO[]> respEntity=restTemplate.exchange(uri, HttpMethod.GET,entity, CountryDTO[].class);
	    CountryDTO[] x = respEntity.getBody();
	    ///*
	    for( CountryDTO c: x ) {
	    	System.out.println("--------CONTRY:v"+c.getName() +" -------");
	    	System.out.println("Name;" + c.getName());
	    	System.out.println("Population"+ c.getPopulation());
	    	System.out.println("Code 2"+ c.getAlpha2Code());
	    	

	    	for(String s: c.getCurrencies()) {
	    		System.out.println("Currency:" + s);
	    		
	    	}
	    	
	    	for(String f:c.getLanguages() ) {
	    		System.out.println("Languages:" + f);
	    		
	    	}
	    	
	    	
	    	
	    	System.out.println("---------------------");
	    	
	    }
	    return x;
	}

}
